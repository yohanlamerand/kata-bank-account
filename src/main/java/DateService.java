import java.time.LocalDate;

interface DateService {
    LocalDate getDate();
}
